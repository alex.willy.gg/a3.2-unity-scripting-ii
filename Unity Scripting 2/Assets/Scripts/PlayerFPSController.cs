﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class PlayerFPSController : MonoBehaviour
{
    public GameObject camerasParent;
    public float walkSpeed = 5f;
    public float hRotationSpeed = 100f;
    public float vRotationSpeed = 80f;

    // Start is called before the first frame update
    void Start()
    {
        //Hide and lock mouse cursor.
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;

        GameObject.Find("Capsule").gameObject.SetActive(false);
    }
    // Update is called once per frame
    void Update()
    {
        //Movement
        movement();
        //Rotation
        rotation();
    }

    private void movement()
    {
        float hMovement = Input.GetAxis("Horizontal");
        float vMovement = Input.GetAxis("Vertical");

        Vector3 movementDirection = hMovement * Vector3.right + vMovement * Vector3.forward;
        transform.Translate(movementDirection * (walkSpeed * Time.deltaTime));
    }
    private void rotation()
    {
        float vCamRotation = Input.GetAxis("Mouse Y") * vRotationSpeed * Time.deltaTime;
        float hPlayerRotation = Input.GetAxis("Mouse X") * hRotationSpeed * Time.deltaTime;

        transform.Rotate(0f, hPlayerRotation, 0f);
        camerasParent.transform.Rotate(-vCamRotation, 0f, 0f);
    }
}
